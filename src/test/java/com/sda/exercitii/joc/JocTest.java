package com.sda.exercitii.joc;

import com.sda.exercitiu.exceptions.NoMoneyException;
import com.sda.exercitiu.joc.Joc;
import com.sda.exercitiu.jucator.Jucator;
import com.sda.exercitiu.jucator.Zar;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Random;

import static org.junit.Assert.assertEquals;

public class JocTest {

    private Joc joc;
    private Jucator jucator1;
    private Jucator jucator2;
    private Random random;

    @Before
    public void before(){
        jucator1 = new Jucator("Ion", 500);
        jucator2 = new Jucator("Mihai", 800);
        random = Mockito.mock(Random.class);
        Zar zar = new Zar(random);
        joc = new Joc(jucator1,jucator2, zar);
    }

    @Test(expected = NoMoneyException.class)
    public void test_play_mizaPreaMare(){
        //when
        joc.play(700);
        //then throw exception
    }

    @Test
    public void test_play_remiza_success(){
        //given
        Mockito.when(random.nextInt(6)).thenReturn(3,4,5,2);
        //when
        joc.play(300);

        //then
        assertEquals(500,jucator1.getSuma());
        assertEquals(800,jucator2.getSuma());
    }

    @Test
    public void test_play_win_success() {
        //given
        Mockito.when(random.nextInt(6)).thenReturn(4, 4, 5, 2);
        //when
        joc.play(200);
        //then
        assertEquals(700, jucator1.getSuma());
        assertEquals(600, jucator2.getSuma());
    }
}

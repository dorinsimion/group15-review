package com.sda.exercitiu.jucator;

import com.sda.exercitiu.exceptions.NoMoneyException;
import org.junit.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class JucatorTest {

    private Jucator jucator;

    @Before
    public void before(){
        //given
        jucator = new Jucator("Mihai",400);
    }

    @Test(expected = NoMoneyException.class)
    public void testUpdateazaSuma_pierderePreaMare(){
        //when
        jucator.updateazaSuma(-500);
        //then exception
    }

    @Test
    public void testUpdateazaSuma_success(){
        //when
        jucator.updateazaSuma(100);
        //then
        assertThat(jucator.getSuma()).isEqualTo(500);
        assertThat(jucator.getIstoric())
                .containsSequence(400,500)
                .hasSize(2);
    }

    @Test
    public void test_aruncaZar(){
        //given
        //mock cu metoda, aveati si varianta cu annotation
        Zar zar = mock(Zar.class);
        //prima oara returneaza 3, a doua oara 4
        when(zar.aruncaZar()).thenReturn(3,4);

        //when
        int result = jucator.aruncaZaruri(zar);

        //then
        assertThat(result).isEqualTo(7);
    }

    @Test
    public void test_getCastiguri(){
        //given
        //adaugam niste tranzactii
        jucator.updateazaSuma(200);
        jucator.updateazaSuma(-100);
        jucator.updateazaSuma(50);
        //when
        int result = jucator.getCastiguri();
        //then
        assertThat(result).isEqualTo(150);
    }

    @Test
    public void test_compareTo(){
        //given
        Jucator altJucator = new Jucator("Ion",300);
        //when
        int result = jucator.compareTo(altJucator);
        //then
        assertThat(result).isEqualTo(100);
    }
}

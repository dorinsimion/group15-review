package com.sda.exercitiu.jucator;

import java.util.Random;

public class Zar {
    private Random random;

    public Zar(Random random) {
        this.random = random;
    }

    int aruncaZar(){
        return random.nextInt(6)+1;
    }
}

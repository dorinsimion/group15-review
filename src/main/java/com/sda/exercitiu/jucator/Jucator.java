package com.sda.exercitiu.jucator;

import com.sda.exercitiu.exceptions.NoMoneyException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class Jucator implements Comparable<Jucator> {
    private final String nume;
    private int suma;
    private final List<Integer> istoric;

    public Jucator(String nume, int suma) {
        this.nume = nume;
        this.suma = suma;
        istoric = new ArrayList<>();
        istoric.add(suma);
    }

    public String getNume() {
        return nume;
    }

    public int getSuma() {
        return suma;
    }

    public List<Integer> getIstoric() {
        return Collections.unmodifiableList(istoric);
    }

    public void updateazaSuma(int miza){
        if(suma+miza<0)
            throw new NoMoneyException("Fonduri insuficiente!");
        suma+=miza;
        istoric.add(suma);
    }

    public int aruncaZaruri(Zar zar){
        int zar1 = zar.aruncaZar();
        int zar2 = zar.aruncaZar();
        System.out.println(nume + " a aruncat "+ zar1+" "+zar2);
        return zar1+zar2;
    }

    public void afiseazaIstoric(){
        System.out.println("Jucatorul "+ nume + "are istoricul:");
        Iterator<Integer> iterator = istoric.iterator();
        int count=1;
        while(iterator.hasNext()){
            System.out.println("\t"+(count++)+". "+iterator.next());
        }
    }

    @Override
    public String toString() {
        return String.format("%s are %d lei!",nume,suma);
    }

    @Override
    public int compareTo(Jucator o) {
        return suma-o.suma;
    }

    public int getCastiguri(){
        return istoric.get(istoric.size()-1)-istoric.get(0);
    }
}

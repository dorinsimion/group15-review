package com.sda.exercitiu;

import com.sda.exercitiu.exceptions.NoMoneyException;
import com.sda.exercitiu.joc.Joc;
import com.sda.exercitiu.jucator.Jucator;
import com.sda.exercitiu.jucator.Zar;

import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

public class JocDemo {
    private static final String ANSI_RESET = "\u001B[0m";
    private static final String ANSI_RED = "\u001B[31m";
    private static final String ANSI_CYAN = "\u001B[36m";

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String line;
        Jucator jucator1 = new Jucator("Ion",400);
        Jucator jucator2 = new Jucator("Mihai",600);
        Zar zar= new Zar(new Random());
        Joc joc = new Joc(jucator1,jucator2,zar);
        do{
            try {
                System.out.println("\nIntroduceti miza:");
                int miza = scanner.nextInt();
                joc.play(miza);
            } catch(InputMismatchException e){
                System.out.println(ANSI_RED+"\nMiza trebuie sa fie un numar intreg"+ANSI_RESET);
            }catch (NoMoneyException e){
                System.out.println(ANSI_RED+"\nUnul din jucatori nu are suficienti bani!"+ANSI_RESET);
            } finally {
                scanner.nextLine();
            }
            System.out.println("\nContinuati?(Y/N)");
            line = scanner.nextLine();
        }while(line.toUpperCase().equals("Y"));

        int wins = jucator1.getCastiguri();
        //schimbam culoare de output
        System.out.println(ANSI_CYAN);
        if(wins<0)
            System.out.println(jucator2.getNume() + " este castigatorul");
        else if(wins>0){
            System.out.println(jucator1.getNume() + " este castigatorul");
        }else {
            System.out.println("Egalitate!");
        }

        if(wins!=0){
            System.out.println("Suma castigata " + Math.abs(wins));
        }
    }
}

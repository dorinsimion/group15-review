package com.sda.exercitiu.joc;

import com.sda.exercitiu.exceptions.NoMoneyException;
import com.sda.exercitiu.jucator.Jucator;
import com.sda.exercitiu.jucator.Zar;

public class Joc {
    private int runda;
    private Jucator jucator1;
    private Jucator jucator2;
    private int miza;
    private Zar zar;

    public Joc(Jucator jucator1, Jucator jucator2,Zar zar) {
        this.zar = zar;
        this.jucator1 = jucator1;
        this.jucator2 = jucator2;
    }

    public void play(int miza){
        seteazaMiza(miza);
        rundaUrmatoare();
        System.out.printf("***** Runda %s *****\n",runda);
        System.out.printf("Miza %d\n",miza);
        int suma1 = jucator1.aruncaZaruri(zar);
        int suma2 = jucator2.aruncaZaruri(zar);
        updateazaJucatori(suma1,suma2);
        System.out.println(jucator1);
        System.out.println(jucator2);
    }

    private void seteazaMiza(int miza) {
        if (valideazaMiza(miza))
            this.miza = miza;
        else
            throw new NoMoneyException("Unul din jucatori nu are suficienti bani!");
    }

    private boolean valideazaMiza(int miza) {
        return miza<jucator1.getSuma() && miza < jucator2.getSuma();
    }

    private void rundaUrmatoare(){
        runda++;
    }

    private void updateazaJucatori(int suma1, int suma2) {
        if(suma1<suma2){
            updateazaJucator(jucator1,jucator2);
        } else if (suma1>suma2){
            updateazaJucator(jucator2, jucator1);
        } else{
            System.out.println("Remiza");
        }
    }

    private void updateazaJucator(Jucator j1, Jucator j2) {
        j1.updateazaSuma(-miza);
        j2.updateazaSuma(miza);
        System.out.println("Castigatorul este " + j2.getNume());
    }
}

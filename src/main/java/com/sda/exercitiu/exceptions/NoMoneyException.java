package com.sda.exercitiu.exceptions;

public class NoMoneyException extends RuntimeException {

    public NoMoneyException(String message){
        super(message);
    }
}
